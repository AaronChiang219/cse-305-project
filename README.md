# Dating Website #

Dating website created with MySQL, Java Servlets, and JDBC

### Description ###

 The basic idea behind your on-line dating system is that it will allow customers to use the web to browse/search the contents of your database (at least that part you want the customer to see) and to meet people they would be interested in dating. 
 In particular, your web site should allow users to browse other user profiles and to contact those users they are interested in dating to see if the interest is mutual.
 It should also allow a user to broadcast his/her current geo-location to see if there is another user in the area interested in meeting for e.g. a cup of coffee.

Additionally, your system should allow a user to "like" (as in facebook) another user's profile, and to alert two users if they like each other's profiles.
If you have downloaded the tinder app, you will see that it provides a similar feature.
Moreover, your system should support a Rate a Date feature that allows a user to rate a date he/she had with another user, and to make date ratings available for other users to see.
Finally, your system should support Referrals, where one user (User A) refers another user (User B) to a third user (User C), so that User C can go on a "blind date" with User B. 

### How do I get set up? ###

Download and install MySQL along with the requirements in this [link](https://o7planning.org/en/10285/create-a-simple-java-web-application-using-servlet-jsp-and-jdbc)

