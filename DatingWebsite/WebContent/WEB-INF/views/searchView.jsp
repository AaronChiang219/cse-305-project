<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
 <head>
	 <jsp:include page="include.jsp" />
    <meta charset="UTF-8">
    <title>Product List</title>
 </head>
 <body>
 
    <jsp:include page="_header.jsp"></jsp:include>
 
    <h2 class = "display-4 text-center">Available Profiles</h2>
 
    <p style="color: red;">${errorString}</p>
 
    <table class = "table table-striped" border="1">
    <thead-light> 
       <tr>
          <th scope="col">Picture</th>
          <th scope="col">ProfileID</th>
          <th scope="col">Age</th>
          <th scope="col">Weight</th>
          <th scope="col">Height</th>
       </tr>
       </thead>
       <tbody>
       <c:forEach items="${profileList}" var="profile" >
          <tr>
          	 <td> <img style ="max-width: auto; height: 100px;" src = "${pageContext.request.contextPath }/image/${profile.picture }" alt = ""/>
             <td><a href="${pageContext.request.contextPath}/profile/${profile.userName}">${profile.userName}</a></td>
             <td>${profile.age}</td>
             <td>${profile.weight}</td>
             <td>${profile.height}</td>
          </tr>
       </c:forEach>
       </tbody>
    </table>
 
 
    <jsp:include page="_footer.jsp"></jsp:include>
 
 </body>
</html>