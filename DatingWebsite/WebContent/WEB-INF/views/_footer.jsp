<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"%>
 
<div>
    <footer class = "container-fluid text-center">
        <p>&copy; Bonbon 2018. All Rights Reserved.</p>
        <ul class="list-inline">
          <li class="list-inline-item">
            <a href="https://docs.google.com/document/d/e/2PACX-1vQq7m1FbsiHETFkfFGW2DrEwrwvJU0JIIRnbOKnah43G3X0Gq7SXq-ehfCSjOnT3MdNUMfxcyyj-EbH/pub">Documentation</a>
          </li>
          <li class="list-inline-item">
            <a href="https://docs.google.com/document/d/e/2PACX-1vQIOi1dTKJOFhNRltZvSps-JZmvRNmUQC1M8O3CaD-vsWW5X8j6ibT4HL7RGQRiIxF380erMz1TM2Qb/pub">Help</a>
          </li>
        </ul>
    </footer>
 
</div>