<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
   <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
   <nav class="navbar navbar-expand-md navbar-light sticky-top">
<div class="container-fluid">
	<a class="navbar-brand" href="#"><img id="logo" src="images/logo.png"></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
	<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarResponsive">
	<ul class="navbar-nav ml-auto">
	<li class="nav-item">
	<a class="nav-link" href="${pageContext.request.contextPath}/">Home</a>
	</li>
	<li class="nav-item">
	<a class="nav-link" href="${pageContext.request.contextPath}/account">Account</a>
	</li>
	<li class="nav-item">
	<a class="nav-link"  href="${pageContext.request.contextPath}/profile">Profile</a>
	</li>
	<li class="nav-item">
	<a class="nav-link" href="${pageContext.request.contextPath}/likes">Likes</a>
	</li>
	<li class="nav-item">
	<a class="nav-link" href="${pageContext.request.contextPath}/datelist">Dates</a>
	</li>
	<li class="nav-item">
	<a class="nav-link" href="${pageContext.request.contextPath}/register">Register</a>
	</li>
	<li class="nav-item">
	<c:if test="${not empty user}">
	<form method="POST" action="${pageContext.request.contextPath}/logout">
	<label class = "nav-link">Logout
	<input type="submit" name="logout" value="Logout" hidden>
	</label>
      </form>
	</c:if>
	<c:if test="${empty user}">
	<a class="nav-link" href="${pageContext.request.contextPath}/login">Login</a>
	</c:if>
	</li>
	<li class="nav-item">
	<form class ="navbar-form" method = "GET" action = "${pageContext.request.contextPath}/search" role = "search">
	<input class = "nav-link" type = "texet" name="ProfileID" placeholder="Search..."> <br>
	</form>
	</li>
	<li class="nav-item">
	<a class="nav-link" href="${pageContext.request.contextPath}/advsearch">Advanced Search</a>
	</li>
		</ul>
	</div>
	
	<script>
	$(document).ready(function () {
	$('.navbar ul a').click(function(e) {
		  var $this = $(this);
		  if (!$this.hasClass('active')) {
		    $this.addClass('active');
		  }
		 // e.preventDefault();
	});
		});
	</script>
</div>
</nav>