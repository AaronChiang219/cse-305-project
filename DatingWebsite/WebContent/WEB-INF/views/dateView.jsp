<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
 <head>
	 <jsp:include page="include.jsp" />
    <meta charset="UTF-8">
    <title>Date View</title>
 </head>
 <body>
 
    <jsp:include page="_header.jsp"></jsp:include>
 
  <div class="panel panel-info">
	<div class="panel-heading">
	<h3 class="panel-title">Date View</h3>
	</div>
	<div class = "panel-body">
	<div class="row">
	<div class="col-md-3 col-lg-3" align="center"></div>
	<div class="col-md-9 col-lg-9">
	<table class="table table-user information">
	<tbody>

	<tr>
	<td>Profile 1</td>
	<td>${date.profile1 }</td>
	</tr>
	<tr>
	<td>Profile 2</td>
	<td>${date.profile2 }</td>
	</tr>
	<tr>
	<td>Date</td>
	<td>${date.date }</td>
	</tr>
	<tr>
	<td>Time</td>
	<td>${date.time }</td>
	</tr>
	<tr>
	<td>Customer Rep</td>
	<td>${date.custRep }</td>
	</tr>
	<tr>
	<td>Location</td>
	<td>${date.location }</td>
	</tr>
	<tr>
	<td>Cost</td>
	<td>${date.cost }</td>
	</tr>
	<tr>
	<td>Rating 1</td>
	<td>${date.rating1 }</td>
	</tr>
	<tr>
	<td>Rating 2</td>
	<td>${date.rating2 }</td>
	</tr>
	</tbody>
	</table>

	<form method = "POST" action = "${pageContext.request.contextPath}/date">
		Change Rating:
		<div class="form-check form-check-inline">
		  <input class="form-check-input" type="radio" name="rating" id="inlineRadio1" value="1">
		  <label class="form-check-label" for="inlineRadio1">1</label>
		</div>
		<div class="form-check form-check-inline">
		  <input class="form-check-input" type="radio" name="rating" id="inlineRadio2" value="2">
		  <label class="form-check-label" for="inlineRadio2">2</label>
		</div>
		<div class="form-check form-check-inline">
		  <input class="form-check-input" type="radio" name="rating" id="inlineRadio3" value="3">
		  <label class="form-check-label" for="inlineRadio3">3</label>
		</div>
		<div class="form-check form-check-inline">
		  <input class="form-check-input" type="radio" name="rating" id="inlineRadio3" value="4">
		  <label class="form-check-label" for="inlineRadio3">4</label>
		</div>
		<div class="form-check form-check-inline">
		  <input class="form-check-input" type="radio" name="rating" id="inlineRadio3" value="5">
		  <label class="form-check-label" for="inlineRadio3">5</label>
		</div>
		<div class="form-check form-check-inline">
		  <button type="submit" class="btn btn-primary">Submit</button>
		</div>
		<input type="hidden" name = "profile1" value = "${date.profile1 }"/>
		<input type="hidden" name = "profile2" value = "${date.profile2 }"/>
		<input type="hidden" name = "date" value = "${date.date }"/>
		<input type="hidden" name = "time" value = "${date.time}"/>
		<c:choose>
		<c:when test = "${user.userName == date.profile1 }">
			<input type="hidden" name = "type" value = "rating1"/>
			<input type="hidden" name = "rating2" value = "${date.rating2 }"/>
		</c:when>
		<c:when test = "${user.userName == date.profile2 }">
			<input type="hidden" name = "type" value = "rating2"/>
			<input type="hidden" name = "rating1" value = "${date.rating1 }"/>
		</c:when>
		</c:choose>
	</form>

	<div id="" style="overflow:auto; height:400px;">
		Comment:  <br />
		${date.comment }
    </div>
	<form method = "POST" action = "${pageContext.request.contextPath}/date">
		<input type="hidden" name = "type" value = "comment"/>
		<input type="hidden" name = "original" value = "${date.comment }"/>
		<input type="hidden" name = "profile1" value = "${date.profile1 }"/>
		<input type="hidden" name = "profile2" value = "${date.profile2 }"/>
		<input type="hidden" name = "date" value = "${date.date }"/>
		<input type="hidden" name = "time" value = "${date.time}"/>
		<div class = "form-group">
		<textarea class = "form-control" rows = "4" cols = "50" name = "comment"></textarea>
		<br>
		<input class = "btn btn-primary" type = "Submit" value = "Send"/>
		</div>
	</form> 

	</div>
	</div>
	</div>
	</div>


<jsp:include page="_footer.jsp"></jsp:include>
 </body>
</html>