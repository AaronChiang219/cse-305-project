<!DOCTYPE html>
<html>
   <head>
	 <jsp:include page="include.jsp" />
      <meta charset="UTF-8">
      <title>Register</title>
   </head>
   <body>
      <jsp:include page="_header.jsp"></jsp:include>
 
      <h3>Registration Page</h3>
      <p style="color: red;">${errorString}</p>
 
 
      <form method="POST" action="${pageContext.request.contextPath}/register">
         <table class = "table" border="0">
            <tr>
               <td>User Name</td>
               <td><input type="text" name="userName" value= "" /> </td>
            </tr>
            <tr>
               <td>Password</td>
               <td><input type="text" name="password" value= "" /> </td>
            </tr>
            <tr>
               <td>SSN</td>
               <td><input type="text" name="SSN" value= "" /> </td>
            </tr>
            <tr>
               <td>Credit Card</td>
               <td><input type="text" name="CCN" value= "" /> </td>
            </tr>
            <tr>
               <td colspan ="2">
                  <input type="submit" value= "Submit" />
                  <a href="${pageContext.request.contextPath}/">Cancel</a>
               </td>
            </tr>
         </table>
      </form>
 
 
      <jsp:include page="_footer.jsp"></jsp:include>
   </body>
</html>