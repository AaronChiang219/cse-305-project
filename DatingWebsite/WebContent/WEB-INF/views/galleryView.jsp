<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
  <head>
	 <jsp:include page="include.jsp" />
     <meta charset="UTF-8">
     <title>Gallery Page</title>
  </head>
  <body>

     <jsp:include page="_header.jsp"></jsp:include>
   
      <h3>Gallery View</h3>
     
		<c:forEach var="img" items="${imageUrlList}">
			<a href="${pageContext.request.contextPath }/image/${img }">
				<img style ="max-width: auto; height: 300px;" src="${pageContext.request.contextPath }/image/${img }" alt = "${img }" >
			</a>
		</c:forEach> 

     <jsp:include page="_footer.jsp"></jsp:include>

  </body>
</html>