<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
 <head>
	 <jsp:include page="include.jsp" />
    <meta charset="UTF-8">
    <title>Profile View</title>
 </head>
 <body>

    <jsp:include page="_header.jsp"></jsp:include>
    <script>document.getElementById("logo").src = "http://localhost:8080/DatingWebsite/images/logo.png"; </script>
    <!-- User Profile  -->
    <div class="container">
      <div class="row">
       <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
   
   
          <div class="panel panel-info">
            <div class="panel-heading">
            <h3 class="panel-title"> ${profile.userName}</h3>
            </div>
            <div class = "panel-body">
            <div class="row">
            <div class="col-md-3 col-lg-3" align="center"> <img alt="User Pic" src="${pageContext.request.contextPath }/image/${profile.picture }" class="img-circle img-responsive" style ="max-width: 100%; height: auto;"></div>
            <div class="col-md-9 col-lg-9">
            <table class="table table-user information">
            <tbody>
            	<tr>
            		<td>Username:</td>
            		<td>${profile.userName}</td>
            	</tr>
            	<tr>
            		<td>Gender:</td>
            		<td>${profile.gender}</td>
            	</tr>
            	<tr>
            		<td>Age:</td>
            		<td>${profile.age}</td>
            	</tr>
            	<tr>
            		<td>Hobbies:</td>
            		<td>${profile.hobbies}</td>
            	</tr>
            	<tr>
            		<td>Height:</td>
            		<td>${profile.height} ft</td>
            	</tr>
            	<tr>
            		<td>Weight:</td>
            		<td>${profile.weight} lbs</td>
            	</tr>
            	<tr>
            		<td>Hair Color:</td>
            		<td>${profile.hair}</td>
            	</tr>
            	</tbody>
            	</table>
            	<form method = "GET" action = "${pageContext.request.contextPath }/gallery">
				<input type="hidden" name="name" value="${profile.userName}"/>
				<button class="btn btn-primary" type="submit" > View Pictures </button> 
            	</form>
				<c:if test="${not empty user}">
					<c:if test="${profile.userName == user.userName}">
						<form method="POST" action="${pageContext.request.contextPath}/upload" enctype="multipart/form-data">
							<br>
							<label class = "btn btn-primary">
							Select File<input type="file" name = "file" value = "Select" hidden/> 
							</label>
							<label class = "btn btn-primary">
							Upload<input type="Submit" value= "Upload" hidden/>
							</label>
							<input type="hidden" name="name" value="${profile.userName}"/>
						</form> <br/>
					</c:if>
				</c:if>
				<c:if test="${profile.userName == user.userName }">
					<c:if test="${not empty imageUrlList }">
						<form method="POST" action = "${pageContext.request.contextPath}/pchange">
							Change Profile Picture <br>
							<select name = "item" class = "custom-select">
							<c:forEach var="img" items="${imageUrlList}">
							<option value = "${img }">${img }</option>
							</c:forEach>
							</select>
							<label class = "btn btn-primary">
							Submit<input type="submit" value="Submit" hidden>
							</label>
							<br/>
							<input type="hidden" name = "prof" value = "${profile.userName }"/>
						</form>
					</c:if>
				</c:if>
				<c:if test="${not empty user}">
					<c:if test="${profile.userName == user.userName}">
						<form method="GET" action="${pageContext.request.contextPath}/edit">
							<label class = "btn btn-primary">
							Edit<input type="Submit" value= "Edit" hidden/> 
							</label>
						</form>
					</c:if>
					<c:if test="${profile.userName != user.userName}">
						<form method="POST" action="">
							<br>
							<label class = "btn btn-primary">
							Like<input type="Submit" value = "Like" hidden /> 
							</label>
							<input type="hidden" name="Liker" value="${user.userName}"/>
							<input type="hidden" name="Likee" value="${profile.userName}"/>
						</form>
						<form method="POST" action="${pageContext.request.contextPath }/likes">
							<label class = "btn btn-primary">
							Unlike<input type="Submit" value = "Unlike" hidden /> 
							</label>
							<input type="hidden" name="Liker" value="${user.userName}"/>
							<input type="hidden" name="Likee" value="${profile.userName}"/>
						</form>
					</c:if>
					<br>
					<c:if test="${profile.userName != user.userName}">
						<form method="GET" action="${pageContext.request.contextPath }/crdate">
							<label class = "btn btn-primary">
							Request Date<input type="Submit" value = "Request Date" hidden /> 
							</label>
							<input type="hidden" name="prof1" value="${user.userName}"/>
							<input type="hidden" name="prof2" value="${profile.userName}"/>
						</form>
					</c:if>
					<c:if test="${profile.userName != user.userName}">
						<form method="GET" action="${pageContext.request.contextPath }/crdate">
							Refer a friend by ProfileID! <br>
							<input class="form-control" type="text" name = "prof1" value ="" />
							<input type="hidden" name="prof2" value="${profile.userName}"/>
							<label class = "btn btn-primary">
							Refer a Friend<input type="Submit" value = "Refer a Friend" hidden/> 
							</label>
						</form>
					</c:if>
				</c:if>
    </div>
    </div>
    </div>
                  </div>
                  </div>
                  </div>
                  </div>

	<jsp:include page="_footer.jsp"></jsp:include>
 
 </body>
</html>