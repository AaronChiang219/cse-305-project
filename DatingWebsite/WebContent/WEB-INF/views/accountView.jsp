<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
 <head>
	 <jsp:include page="include.jsp" />
    <meta charset="UTF-8">
    <title>Profile View</title>
 </head>
 <body>
 
    <jsp:include page="_header.jsp"></jsp:include>

 <div class="container">
      <div class="row">
       <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
   
   
          <div class="panel panel-info">
            <div class="panel-heading">
            <h3 class="panel-title"> ${profile.userName}</h3>
            </div>
            <div class = "panel-body">
            <div class="row">
            <div class="col-md-3 col-lg-3" align="center"></div>
            <div class="col-md-9 col-lg-9">
            <table class="table table-user information">
            <tbody>
            <tr>
            <td>User Name</td>
            <td>${account.userName } </td>
            </tr>
            <tr>
            <td>First Name</td>
            <td> ${account.firstName } </td>
            </tr>
            <tr>
            <td>Last Name</td>
            <td> ${account.lastName } </td>
            </tr>
            <tr>
            <td>Street</td>
            <td> ${account.street } </td>
            </tr>
            <tr>
            <td>City</td>
            <td> ${account.city } </td>
            </tr>
            <tr>
            <td>State</td>
            <td> ${account.state } </td>
            </tr>
            <tr>
            <td>Zip</td>
            <td> ${account.zip } </td>
            </tr>
            <tr>
            <td>Email</td>
            <td> ${account.email } </td>
            </tr>
            <tr>
            <td>Phone</td>
            <td> ${account.phone } </td>
            </tr>
            <tr>
            <td>Profile Placement Priority</td>
            <td> ${account.ppp } </td>
            </tr>
            </tbody>
            </table>
			<c:if test="${not empty account}">
				<form method="GET" action="${pageContext.request.contextPath}/actedit">
				<label class = "btn btn-primary">
				Edit<input type="submit" hidden /> 
				</label>
				</form>
			</c:if>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
 
    
	<jsp:include page="_footer.jsp"></jsp:include>
 
 </body>
</html>