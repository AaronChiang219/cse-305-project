<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
 <head>
	 <jsp:include page="include.jsp" />
    <meta charset="UTF-8">
    <title>Profile View</title>
 </head>
 <body>
 
    <jsp:include page="_header.jsp"></jsp:include>
  <div class="container">
      <div class="row">
       <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
   
   
          <div class="panel panel-info">
            <div class="panel-heading">
            <h3 class="panel-title"> ${profile.userName}</h3>
            </div>
            <div class = "panel-body">
            <div class="row">
            <div class="col-md-3 col-lg-3" align="center"></div>
            <div class="col-md-9 col-lg-9">
            <table class="table table-user information">
            <tbody>
            <tr>
            <td>First Name</td>
            <td> ${user.firstName } </td>
            </tr>
            <tr>
            <td>Last Name</td>
            <td> ${user.lastName } </td>
            </tr>
            <tr>
            <td>Street</td>
            <td> ${user.street } </td>
            </tr>
            <tr>
            <td>City</td>
            <td> ${user.city } </td>
            </tr>
            <tr>
            <td>State</td>
            <td> ${user.state } </td>
            </tr>
            <tr>
            <td>Zip</td>
            <td> ${user.zip } </td>
            </tr>
            <tr>
            <td>Email</td>
            <td> ${user.email } </td>
            </tr>
            <tr>
            <td>Phone</td>
            <td> ${user.phone } </td>
            </tr>
            <tr>
            <td>Wage</td>
            <td> ${user.wage } </td>
            </tr>
            <tr>
            <td>Role</td>
            <td> ${user.role } </td>
            </tr>
            </tbody>
            </table>
			<c:if test="${not empty user}">
				<form method="GET" action="${pageContext.request.contextPath}/epedit">
					<input type="Submit" value= "Edit" /> 
					<input type="hidden" value = "${user.email }" name = "userName" >
					<input type="hidden" value = "${user.password }" name = "password" >
				</form>
			</c:if>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
	<jsp:include page="_footer.jsp"></jsp:include>
 
 </body>
</html>