<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
 <head>
	 <jsp:include page="include.jsp" />
    <meta charset="UTF-8">
    <title>Account View</title>
 </head>
 <body>

    <jsp:include page="_header.jsp"></jsp:include>
 
 
	<form method = "POST" action = "${pageContext.request.contextPath}/actedit">
	<div class = "form-group">
	<label for="username">Username</label>
		<input type = "text" name = "username" class ="form-control" value = "${account.userName }" disabled>
	</div>
	<div class = "form-row">
		<div class = "col">
		<label for="inputfirstName">First Name</label>
			<input type = "text" name = "firstName" class="form-control" value = "${account.firstName }">
		</div>
		<div class ="col">
		<label for="inputLastName">Last Name</label>
			<input type = "text" name = "lastName" class="form-control" value = "${account.lastName }">
		</div>
	</div>
	<div class = "form-group">
	<label for="currentPassword">Current Password</label>
		<input type = "text" class ="form-control" value = "${account.password }" disabled>
	</div>
	<div class = "form-group">
	<label for="newPassword">New Password</label>
		<input type = "password" name = "password" class ="form-control" placeholder = "Password">
	</div>
	<div class = "form-group">
	<label for="CCNumber">Credit Card Number</label>
		<input type = "text" name = "ccn" class ="form-control" placeholder = "Credit Card Number">
	</div>
	<div class = "form-row">
	<div class = "col">
	<label for="Street">Street</label>
		<input type="text" name = "street" class ="form-control" value ="${account.street }">
	</div>
	<div class = "col">
	<label for="City">City</label>
		<input type="text" name = "city" class ="form-control" value ="${account.city }">
	</div>
	<div class = "col">
	<label for="state">State</label>
		<input type="text" name = "state" class ="form-control" value ="${account.state }">
	</div>
	<div class = "col">
	<label for="zipcode">Zip Code</label>
		<input type="text" name = "zip" class ="form-control" value ="${account.zip }">
	</div>
	</div>
	<div class ="form-row">
	<div class = "col">
		<label for="email">Email</label>
		<input type="text" name = "email" class="form-control" value="${account.email}">
	</div>
	<div class = "col">
		<label for="phone">Phone</label>
		<input type="text" name = "phone" class="form-control" value="${account.phone}" >
	</div>
	</div>
	<select class="form-control"name = "ppp" >
    <option>Super User ($100/yr)</option>
    <option> Good User ($50/yr)</option>
    <option> Free User ($0/yr)</option>
    </select>
<label class ="btn btn-primary">Submit<input type="Submit" value="submit" hidden></label>
<label class = "btn"><a href = "${pageContext.request.contextPath}/" >Cancel</a></label>
</form>


    <jsp:include page="_footer.jsp"></jsp:include>
 
 </body>
</html>