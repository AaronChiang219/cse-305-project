<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
 <head>
	 <jsp:include page="include.jsp" />
    <meta charset="UTF-8">
    <title>Account View</title>
 </head>
 <body>
 
    <jsp:include page="_header.jsp"></jsp:include>
 
 
	<form method = "POST" action = "${pageContext.request.contextPath}/crdate">
	<div class = "form-group">
	<label for="username">Profile1</label>
	<input type = text class = "form-control" value = "${prof1}" disabled>
	</div>
	<div class = "form-group">
		<label for="ages">Profile2</label>
		<input type = "text" class = "form-control" value = "${prof2}" disabled/>
	</div>
	<div class = "form-group">
		<label for="hobby">Date and Time</label>
		<input type = "datetime-local" class = "form-control" name = "time" value = ""/>
	</div>
	<div class = "form-group">
		<label for="h">Location</label>
		<input type = "text" class = "form-control" name = "location" value = ""/>
	</div>
	<input type="hidden" name = "prof1" value = "${prof1 }"/>
	<input type="hidden" name = "prof2" value = "${prof2 }"/>
	<label class ="btn btn-primary">Submit<input type="Submit" value="submit" hidden></label>
	<label class = "btn"><a href = "${pageContext.request.contextPath}/" >Cancel</a></label>
	
	 </form>

    <jsp:include page="_footer.jsp"></jsp:include>
 
 </body>
</html>