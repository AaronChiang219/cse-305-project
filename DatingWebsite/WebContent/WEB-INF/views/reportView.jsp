<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>  
<!DOCTYPE html>
<html>
 <head>
	 <jsp:include page="include.jsp" />
    <meta charset="UTF-8">
    <title>Date List</title>
 </head>
 <body>
 
    <jsp:include page="_header.jsp"></jsp:include>
 
    <h3>Report</h3>
 
    <p style="color: red;">${errorString}</p>
    <table class = "table table-striped" border="1"> 
    <thead-light> 
       <tr>
          <th>Date</th>
          <th>Cost</th>
       </tr>
       </thead-light>
       <tbody>
       <c:set var = "total" value = "0"/>
       <c:forEach items="${dateList}" var="date" >
          <tr> 
             <td>${date.date}</td>
             <td><fmt:formatNumber value="${date.cost}" type = "currency"/></td>
          </tr>
          <c:set var = "total" value = "${total+ date.cost}"/>
       </c:forEach>
       </tbody>
    </table>
    Total Earnings :
    <fmt:formatNumber value = "${total }" type = "currency"/>
 
    <jsp:include page="_footer.jsp"></jsp:include>
 
 </body>
</html>