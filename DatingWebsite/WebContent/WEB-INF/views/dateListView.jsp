<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
 <head>
	 <jsp:include page="include.jsp" />
    <meta charset="UTF-8">
    <title>Date List</title>
 </head>
 <body>
 
    <jsp:include page="_header.jsp"></jsp:include>
 
    <h3>Date List</h3>
 
    <p style="color: red;">${errorString}</p>
 
    <table class = "table table-striped" border="1">
    <thead-light> 
       <tr>
          <th>Link</th>
          <th>Profile 1</th>
          <th>Profile 2</th>
          <th>Date</th>
          <th>Time</th>
          <th>Remove</th>
       </tr>
     </thead-light>
       <tbody>
       <c:forEach items="${dateList}" var="date" >
          <tr>
             <td>
             <form method = "GET" action = "${pageContext.request.contextPath}/date">
			<input type="hidden" name="prof1" value="${date.profile1}"/>
			<input type="hidden" name="prof2" value="${date.profile2}"/>
			<input type="hidden" name="date" value="${date.date}"/>
			<input type="hidden" name="time" value="${date.time}"/>
				<label class = "btn btn-primary">
				 View Date<input type = "submit" value = "View Date" hidden/>
				 </label>
             </form>
             </td>
             <td>${date.profile1}</td>
             <td>${date.profile2}</td>
             <td>${date.date}</td>
             <td>${date.time}</td>
             <td>
             <form method = "POST" action = "${pageContext.request.contextPath}/datelist">
			<input type="hidden" name="prof1" value="${date.profile1}"/>
			<input type="hidden" name="prof2" value="${date.profile2}"/>
			<input type="hidden" name="date" value="${date.date}"/>
			<input type="hidden" name="time" value="${date.time}"/>
				<label class = "btn btn-primary">
			 Delete Date<input type = "submit" value = "Delete Date" hidden/>
			 </label>
             </form>
			  </tr>
       </c:forEach>
       </tbody>
    </table>
 
 
    <jsp:include page="_footer.jsp"></jsp:include>
 
 </body>
</html>