<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
 <head>
	 <jsp:include page="include.jsp" />
    <meta charset="UTF-8">
    <title>Date List</title>
 </head>
 <body>
 
    <jsp:include page="_header.jsp"></jsp:include>
 
    <h3>Employee List</h3>
 
    <p style="color: red;">${errorString}</p>
    <a href="${pageContext.request.contextPath }/epadd">Add Employee</a>
    <table class = "table table-striped" border="1"> 
    <thead-light> 
       <tr>
          <th>Link</th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Role</th>
          <th>Wage</th>
          <th>Email</th>
          <th>Phone</th>
          <th>Delete</th>
       </tr>
       </thead-light>
       <tbody>
       <c:forEach items="${userList}" var="user" >
          <tr> 
          	<td>
		    <form method = "GET" action = "${pageContext.request.contextPath}/epview">
			<input type="hidden" name="email" value="${user.email}"/>
			<input type="hidden" name="password" value="${user.password}"/>
			<label class = "btn btn-primary">
			 View Employee<input type = "submit" value = "View Employee" hidden/>
			 </label>
             </form>
             </td>
             <td>${user.firstName}</td>
             <td>${user.lastName}</td>
             <td>${user.role}</td>
             <td>${user.wage}</td>
             <td>${user.email}</td>
             <td>${user.phone}</td>
          	<td>
		    <form method = "POST" action = "${pageContext.request.contextPath}/epview">
			<input type="hidden" name="email" value="${user.email}"/>
			<input type="hidden" name="password" value="${user.password}"/>
			<label class = "btn btn-primary">
			 Remove Employee<input type = "submit" value = "Remove Employee" hidden/>
			 </label>
             </form>
             </td>
          </tr>
       </c:forEach>
       </tbody>
    </table>
 
 
    <jsp:include page="_footer.jsp"></jsp:include>
 
 </body>
</html>