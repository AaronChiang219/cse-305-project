<!DOCTYPE html>
<html>
   <head>
	 <jsp:include page="include.jsp" />
      <meta charset="UTF-8">
      <title>Register</title>
   </head>
   <body>
      <jsp:include page="_header.jsp"></jsp:include>
 
      <h3>Registration Page</h3>
      <p style="color: red;">${errorString}</p>
 
 
      <form method="POST" action="${pageContext.request.contextPath}/addep">
         <table class = "table" border="0">
            <tr>
               <td>First Name</td>
               <td><input type="text" name="fname" value= "" /> </td>
            </tr>
            <tr>
               <td>Last Name</td>
               <td><input type="text" name="lname" value= "" /> </td>
            </tr>
            <tr>
               <td>Street</td>
               <td><input type="text" name="street" value= "" /> </td>
            </tr>
            <tr>
               <td>City</td>
               <td><input type="text" name="city" value= "" /> </td>
            </tr>
            <tr>
               <td>State</td>
               <td><input type="text" name="state" value= "" /> </td>
            </tr>
            <tr>
               <td>Zipcode</td>
               <td><input type="text" name="zip" value= "" /> </td>
            </tr>
            <tr>
               <td>Email</td>
               <td><input type="text" name="email" value= "" /> </td>
            </tr>
            <tr>
               <td>Telephone</td>
               <td><input type="text" name="phone" value= "" /> </td>
            </tr>
            <tr>
               <td>SSN</td>
               <td><input type="text" name="SSN" value= "" /> </td>
            </tr>
            <tr>
               <td>Password</td>
               <td><input type="text" name="pass" value= "" /> </td>
            </tr>
            <tr>
               <td>Role</td>
               <td><input type="text" name="role" value= "" /> </td>
            </tr>
            <tr>
               <td>HourlyRate</td>
               <td><input type="text" name="rate" value= "" /> </td>
            </tr>
            <tr>
               <td colspan ="2">
                  <input type="submit" value= "Submit" />
                  <a href="${pageContext.request.contextPath}/">Cancel</a>
               </td>
            </tr>
         </table>
      </form>
 
 
      <jsp:include page="_footer.jsp"></jsp:include>
   </body>
</html>