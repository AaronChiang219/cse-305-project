<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
 <head>
	 <jsp:include page="include.jsp" />
    <meta charset="UTF-8">
    <title>Advanced Search</title>
 </head>
 <body>
 
    <jsp:include page="_header.jsp"></jsp:include>
 
 	<form method = "POST" action = "${pageContext.request.contextPath}/advsearch">
	<fieldset class="form-group">
    <div class="row">
      <legend class="col-form-label col-sm-2 pt-0">Gender</legend>
      <div class="col-sm-10">
        <div class="form-check">
          <input class="form-check-input" type="radio" name="gender" value="Male">
          <label class="form-check-label" for="maleradio">
            Male
          </label>
        </div>
        <div class="form-check">
          <input class="form-check-input" type="radio" name="gender"value="Female">
          <label class="form-check-label" for="femaleradio">
            Female
          </label>
        </div>
      </div>
  </div>
  </fieldset>
	<div class = "form-group">
	<label for="sage">Start Age</label>
		<input type = "text" name = "ages" class ="form-control" value = "" >
	</div>	
	<div class = "form-group">
	<label for="username">End Age</label>
		<input type = "text" name = "agee" class ="form-control" value = "" >
	</div>	
	<div class = "form-group">
	<label for="username">Hobbies</label>
		<input type = "text" name = "hobbies" class ="form-control" value = "" >
	</div>	
	<div class = "form-group">
	<label for="username">Start Height</label>
		<input type = "text" name = "heights" class ="form-control" value = "" >
	</div>	
	<div class = "form-group">
	<label for="username">End Height</label>
		<input type = "text" name = "heighte" class ="form-control" value = "" >
	</div>	
	<div class = "form-group">
	<label for="username">Start Weight</label>
		<input type = "text" name = "weights" class ="form-control" value = "" >
	</div>	
	<div class = "form-group">
	<label for="username">End Weight</label>
		<input type = "text" name = "weighte" class ="form-control" value = "" >
	</div>	
	<div class = "form-group">
	<label for="username">Hair</label>
		<input type = "text" name = "hair" class ="form-control" value = "" >
	</div>	
	<label class = "btn btn-primary">
	Submit<input type = "submit" value = "submit" hidden/>
	</label>
	<label class = "btn ">
	<a href = "${pageContext.request.contextPath}/advsearch?type=dates">Search by Most Dates</a> <br>
	</label>
	<label class = "btn ">
	<a href = "${pageContext.request.contextPath}/advsearch?type=active">Search by Most Active</a> <br>
	</label>
	<label class = "btn ">
	<a href = "${pageContext.request.contextPath}/advsearch?type=rated">Search by Highest Rated</a> <br>
	</label>
	
 
	<jsp:include page="_footer.jsp"></jsp:include>
 
 </body>
</html>