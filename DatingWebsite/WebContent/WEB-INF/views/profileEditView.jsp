<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
 <head>
	 <jsp:include page="include.jsp" />
    <meta charset="UTF-8">
    <title>Profile View</title>
 </head>
 <body>
 
    <jsp:include page="_header.jsp"></jsp:include>
    <script>document.getElementById("logo").src = "http://localhost:8080/DatingWebsite/images/logo.png"; </script>

	<form method = "POST" action = "${pageContext.request.contextPath}/edit">
	<div class = "form-group">
	<label for="username">Username</label>
	<input type = text class = "form-control" value = "${profile.userName }" disabled>
	</div>
	<fieldset class="form-group">
    <div class="row">
      <legend class="col-form-label col-sm-2 pt-0">Gender</legend>
      <div class="col-sm-10">
        <div class="form-check">
          <input class="form-check-input" type="radio" name="gender" value="Male">
          <label class="form-check-label" for="maleradio">
            Male
          </label>
        </div>
        <div class="form-check">
          <input class="form-check-input" type="radio" name="gender"value="Female">
          <label class="form-check-label" for="femaleradio">
            Female
          </label>
        </div>
      </div>
    </div>
  </fieldset>
	<div class = "form-group">
		<label for="ages">Age</label>
		<input type = "text" class = "form-control" name = "age" value = "${profile.age }"/>
	</div>
	<div class = "form-group">
		<label for="hobby">Hobbies</label>
		<input type = "text" class = "form-control" name = "hobbies" value = "${profile.hobbies }"/>
	</div>
	<div class = "form-group">
		<label for="h">Height</label>
		<input type = "text" class = "form-control" name = "height" value = "${profile.height }"/>
	</div>
	<div class = "form-group">
		<label for="wj">Weight</label>
		<input type = "text" class = "form-control" name = "weight" value = "${profile.weight }"/>
	</div>
	<div class = "form-group">
		<label for="hr">Hair</label>
		<input type = "text" class = "form-control" name = "hair" value = "${profile.hair }"/>
	</div>
	<label class ="btn btn-primary">Submit<input type="Submit" value="submit" hidden></label>
	<label class = "btn"><a href = "${pageContext.request.contextPath}/" >Cancel</a></label>
	 </form>

    <jsp:include page="_footer.jsp"></jsp:include>
 
 </body>
</html>