package org.o7planning.datingwebsite.servlet;

 import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 * Servlet to handle File upload request from Client
 * @author Javin Paul
 */

@WebServlet(urlPatterns = { "/upload","/gallery" })
@MultipartConfig
public class UploadServlet extends HttpServlet {
  
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	Part filePart = request.getPart("file");
    	String profName = request.getParameter("name");
    	String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
    	String uploadFolder = getServletContext().getRealPath("") + "data" + File.separator;
    	File directory = new File(uploadFolder + profName);
    	if (!directory.exists()) {
    		directory.mkdir();
    	}
    	filePart.write(directory.getPath() + File.separator + fileName);
    	
    	response.sendRedirect(request.getContextPath() + "/profile");
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    		throws ServletException, IOException {
    	String profName = request.getParameter("name");
    	String uploadFolder = getServletContext().getRealPath("") + "data" + File.separator;
    	File directory = new File(uploadFolder + profName);
    	List<String> imageUrlList = new ArrayList<String>();
    	if (directory.listFiles() != null) {
			for(File imageFile : directory.listFiles()){
			  String imageFileName = profName + File.separator + imageFile.getName();
			  // add this images name to the list we are building up
			  imageUrlList.add(imageFileName);
			}
    	}
    	request.setAttribute("imageUrlList", imageUrlList);
    	RequestDispatcher dispatcher = request.getServletContext()
                .getRequestDispatcher("/WEB-INF/views/galleryView.jsp");
        dispatcher.forward(request, response);
    }
  
}