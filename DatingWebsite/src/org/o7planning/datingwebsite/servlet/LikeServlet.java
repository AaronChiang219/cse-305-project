package org.o7planning.datingwebsite.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
 
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
 
import org.o7planning.datingwebsite.beans.UserAccount;
import org.o7planning.datingwebsite.beans.ProfileAccount;
import org.o7planning.datingwebsite.utils.DBUtils;
import org.o7planning.datingwebsite.utils.MyUtils;
 
@WebServlet(urlPatterns = { "/likes" })
public class LikeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
 
    public LikeServlet() {
        super();
    }
 
    // Show Login page.
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
 
    	HttpSession session = request.getSession();
		Connection conn = MyUtils.getStoredConnection(request);
        String errorString = null;
        List<ProfileAccount> list = null;

        UserAccount loginedUser = MyUtils.getLoginedUser(session);
        if (loginedUser!=null) {
        	try {
        		list = DBUtils.queryLikes(conn, loginedUser.getUserName());
        	} catch (SQLException e) {
        		e.printStackTrace();
        		errorString = e.getMessage();
        	}
			request.setAttribute("errorString", errorString);
        	request.setAttribute("profileList", list);
			RequestDispatcher dispatcher //
					= this.getServletContext().getRequestDispatcher("/WEB-INF/views/searchView.jsp");
			dispatcher.forward(request, response);
			return;
        } else {
			response.sendRedirect(request.getContextPath() + "/login");
			return;
        }

    }
 
    // When the user enters userName & password, and click Submit.
    // This method will be executed.
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	
    	HttpSession session = request.getSession();
		Connection conn = MyUtils.getStoredConnection(request);
		String Liker = request.getParameter("Liker");
		String Likee = request.getParameter("Likee");
		try {
			DBUtils.removeLike(conn, Liker, Likee);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    	response.sendRedirect(request.getContextPath() + "/likes");
        return;

 
    }
 
}