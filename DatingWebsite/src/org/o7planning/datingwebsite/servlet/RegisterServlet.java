package org.o7planning.datingwebsite.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
 
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
 
import org.o7planning.datingwebsite.beans.UserAccount;
import org.o7planning.datingwebsite.utils.DBUtils;
import org.o7planning.datingwebsite.utils.MyUtils;
 
@WebServlet(urlPatterns = { "/register" })
public class RegisterServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
 
    public RegisterServlet() {
        super();
    }
 
    // Show Login page.
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
 
        // Forward to /WEB-INF/views/registerView.jsp
        // (Users can not access directly into JSP pages placed in WEB-INF)
        RequestDispatcher dispatcher 
                = this.getServletContext().getRequestDispatcher("/WEB-INF/views/registerView.jsp");
 
        dispatcher.forward(request, response);
 
    }
 
    // When the user enters userName & password, and click Submit.
    // This method will be executed.
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String userName = request.getParameter("userName");
        String password = request.getParameter("password");
        String OwnerSSN = request.getParameter("SSN");
        long CCN = Long.parseLong(request.getParameter("CCN"));
 
        UserAccount user = null;
        boolean hasError = false;
        String errorString = null;
 
		Connection conn = MyUtils.getStoredConnection(request);

        if (userName == null || password == null || userName.length() == 0 || password.length() == 0 || OwnerSSN == null || OwnerSSN.length() == 0) {
            hasError = true;
            errorString = "Required username and password and SSN!";
        } else {
            try {
                // Check for valid userName and SSN
                if (DBUtils.ValidSSN(conn, OwnerSSN)) {
                	errorString = "SSN invalid";
                	hasError = true;
                }
                
                if (DBUtils.ValidUserName(conn, userName)) {
                	errorString = "User Name invalid";
                	hasError = true;
                }
 
            } catch (SQLException e) {
                e.printStackTrace();
                hasError = true;
                errorString = e.getMessage();
            }
        }
        // If error, forward to /WEB-INF/views/registerView.jsp
        if (hasError) {
            //user = new UserAccount();
            //user.setUserName(userName);
            //user.setPassword(password);
 
            // Store information in request attribute, before forward.
            request.setAttribute("errorString", errorString);
 
            // Forward to /WEB-INF/views/login.jsp
            RequestDispatcher dispatcher //
                    = this.getServletContext().getRequestDispatcher("/WEB-INF/views/registerView.jsp");
 
            dispatcher.forward(request, response);
        }
        // If no error
        // Redirect to ProfileInfo page
        else {
        	try {
					DBUtils.createAccount(conn, userName, OwnerSSN, CCN, password);
					user = DBUtils.findUser(conn, userName, password);
        	} catch (SQLException e) {
        		e.printStackTrace();
        		return;
        	}
            HttpSession session = request.getSession();
            MyUtils.storeLoginedUser(session, user);
 
			MyUtils.deleteUserCookie(response);
 
            // Redirect to userInfo page.
            response.sendRedirect(request.getContextPath() + "/profile");
        }
    }
 
}