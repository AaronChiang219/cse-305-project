package org.o7planning.datingwebsite.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
 
import org.o7planning.datingwebsite.beans.UserAccount;
import org.o7planning.datingwebsite.beans.ProfileAccount;
import org.o7planning.datingwebsite.utils.DBUtils;
import org.o7planning.datingwebsite.utils.MyUtils;
 
@WebServlet(urlPatterns = { "/edit"})
public class ProfileEditServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
 
    public ProfileEditServlet() {
        super();
    }
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	//modify to get all attributes of current user
        HttpSession session = request.getSession();
 
        // Check User has logged on
        UserAccount loginedUser = MyUtils.getLoginedUser(session);
 
        if (loginedUser!=null) {
        	Connection conn = MyUtils.getStoredConnection(request);
        	ProfileAccount profile = new ProfileAccount();
        	try {
        		profile = DBUtils.findAccount(conn, loginedUser.getUserName());
        	} catch (SQLException e) {
        		e.printStackTrace();
				RequestDispatcher dispatcher //
                    = this.getServletContext().getRequestDispatcher("/WEB-INF/views/loginView.jsp");
				dispatcher.forward(request,response);
				return;
        	}
			request.setAttribute("user", loginedUser);
        	request.setAttribute("profile", profile);
			RequestDispatcher dispatcher //
					= this.getServletContext().getRequestDispatcher("/WEB-INF/views/profileEditView.jsp");
			dispatcher.forward(request, response);
			return;
        } else {
			response.sendRedirect(request.getContextPath() + "/login");
			return;
        }
    }
 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
		HttpSession session = request.getSession();
		Connection conn = MyUtils.getStoredConnection(request);
        UserAccount loginedUser = MyUtils.getLoginedUser(session);
		String ProfileID = loginedUser.getUserName();
    	String Gender = request.getParameter("gender");
    	int Age = Integer.parseInt(request.getParameter("age"));
    	String Hobbies = request.getParameter("hobbies");
    	int Height = Integer.parseInt(request.getParameter("height"));
    	int Weight = Integer.parseInt(request.getParameter("weight"));
    	String Hair = request.getParameter("hair");
    	try {
			DBUtils.updateProfile(conn, ProfileID, Gender, Age, Hobbies, Height, Weight, Hair);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		response.sendRedirect(request.getContextPath() + "/profile");




    }
 
}