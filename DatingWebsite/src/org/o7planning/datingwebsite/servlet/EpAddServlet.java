package org.o7planning.datingwebsite.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
 
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
 
import org.o7planning.datingwebsite.beans.UserAccount;
import org.o7planning.datingwebsite.utils.DBUtils;
import org.o7planning.datingwebsite.utils.MyUtils;
 
@WebServlet(urlPatterns = { "/epadd" })
public class EpAddServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
 
    public EpAddServlet() {
        super();
    }
 
    // Show Login page.
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
 
        // Forward to /WEB-INF/views/registerView.jsp
        // (Users can not access directly into JSP pages placed in WEB-INF)
        RequestDispatcher dispatcher 
                = this.getServletContext().getRequestDispatcher("/WEB-INF/views/employeeAddView.jsp");
 
        dispatcher.forward(request, response);
 
    }
 
    // When the user enters userName & password, and click Submit.
    // This method will be executed.
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	String FirstName = request.getParameter("fname");
    	String LastName = request.getParameter("lname");
    	String Street = request.getParameter("street");
    	String City = request.getParameter("city");
    	String State = request.getParameter("state");
    	String Zip = request.getParameter("zip");
    	String Email = request.getParameter("email");
    	String Phone = request.getParameter("phone");
    	String SSN = request.getParameter("SSN");
    	String Pass = request.getParameter("pass");
    	String Role = request.getParameter("role");
    	String Wage = request.getParameter("rate");
		Connection conn = MyUtils.getStoredConnection(request);
		try {
			DBUtils.addEmployee(conn, FirstName, LastName, Street, City, State, Zip, Email, Phone, SSN, Pass, Role, Wage);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		response.sendRedirect(request.getContextPath()+ "/eplist");
    }
 
}