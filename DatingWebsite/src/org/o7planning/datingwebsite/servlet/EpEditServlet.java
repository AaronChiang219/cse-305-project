package org.o7planning.datingwebsite.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
 
import org.o7planning.datingwebsite.beans.UserAccount;
import org.o7planning.datingwebsite.beans.ProfileAccount;
import org.o7planning.datingwebsite.utils.DBUtils;
import org.o7planning.datingwebsite.utils.MyUtils;
 
@WebServlet(urlPatterns = { "/epedit"})
public class EpEditServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
 
    public EpEditServlet() {
        super();
    }
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	//modify to get all attributes of current user
        HttpSession session = request.getSession();
		Connection conn = MyUtils.getStoredConnection(request);
        // Check User has logged on
        String userName = request.getParameter("userName");
        String password = request.getParameter("password");
        UserAccount employee = null;
		try {
			employee = DBUtils.findEmployee(conn, userName, password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        if (employee!=null) {
        	request.setAttribute("account", employee);
			RequestDispatcher dispatcher //
					= this.getServletContext().getRequestDispatcher("/WEB-INF/views/employeeEditView.jsp");
			dispatcher.forward(request, response);
			return;
        } else {
        	response.sendRedirect(request.getContextPath() + "/login");
        }
        return;
    }
 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
		HttpSession session = request.getSession();
		Connection conn = MyUtils.getStoredConnection(request);
        UserAccount loginedUser = MyUtils.getLoginedUser(session);
		String FirstName = request.getParameter("firstName");
		String LastName = request.getParameter("lastName");
		String Street = request.getParameter("street");
		String City = request.getParameter("city");
		String State = request.getParameter("state");
		int zip = Integer.parseInt(request.getParameter("zip"));
		String Email = request.getParameter("email");
		String Phone = request.getParameter("phone");
		String Wage = request.getParameter("wage");
		String Role = request.getParameter("role");
		String ssn = request.getParameter("ssn");
    	try {
			DBUtils.updateEmployee(conn, FirstName, LastName, Street, City, State, zip, Email, Phone, Wage, Role, ssn);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		response.sendRedirect(request.getContextPath() + "/eplist");
		return;


    }
 
}