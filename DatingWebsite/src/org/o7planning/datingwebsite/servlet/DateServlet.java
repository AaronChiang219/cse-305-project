package org.o7planning.datingwebsite.servlet;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
 
import org.o7planning.datingwebsite.beans.UserAccount;
import org.o7planning.datingwebsite.beans.Date;
import org.o7planning.datingwebsite.beans.ProfileAccount;
import org.o7planning.datingwebsite.utils.DBUtils;
import org.o7planning.datingwebsite.utils.MyUtils;
 
@WebServlet(urlPatterns = { "/date" })
public class DateServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
 
    public DateServlet() {
        super();
    }
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
		Connection conn = MyUtils.getStoredConnection(request);
        UserAccount loginedUser = MyUtils.getLoginedUser(session);
        String profile1 = request.getParameter("prof1");
        String profile2 = request.getParameter("prof2");
        String date = request.getParameter("date");
        String time = request.getParameter("time");
        Date d = new Date();
        try {
        	d = DBUtils.findDate(conn, profile1, profile2, date, time);
        } catch (SQLException e){
        	e.printStackTrace();
			RequestDispatcher dispatcher //
			= this.getServletContext().getRequestDispatcher("/WEB-INF/views/loginView.jsp");
			dispatcher.forward(request,response);
			return;
        }
        request.setAttribute("date", d);
        request.setAttribute("user", loginedUser);
		RequestDispatcher dispatcher //
				= this.getServletContext().getRequestDispatcher("/WEB-INF/views/dateView.jsp");
		dispatcher.forward(request, response);
		return;
	
    }
 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
		Connection conn = MyUtils.getStoredConnection(request);
    	String type = request.getParameter("type");       
    	String profile1 = request.getParameter("profile1");
        String profile2 = request.getParameter("profile2");
        String date = request.getParameter("date");
        String time = request.getParameter("time");
        if (type.equals("rating2")) {
        	try {
        		String rating2 = request.getParameter("rating");
        		String rating1 = request.getParameter("rating1");
        		System.out.println(DBUtils.updateDate(conn, profile1, profile2, date, time, rating1, rating2));
        	} catch (SQLException e) {
        		e.printStackTrace();
        	}
        } else if (type.equals("rating1")) {
        	try {
        		String rating1 = request.getParameter("rating");
        		String rating2 = request.getParameter("rating2");
        		DBUtils.updateDate(conn, profile1, profile2, date, time, rating1, rating2);
        	} catch (SQLException e) {
        		e.printStackTrace();
        	}
        } else if (type.equals("comment")) {
        	try {
        		String comment = request.getParameter("comment");
        		System.out.println(comment);
        		String original = request.getParameter("original");
        		String newcomm = original + "\n" + comment;
        		DBUtils.addComment(conn, profile1, profile2, date, time, newcomm);
        	} catch (SQLException e) {
        		e.printStackTrace();
        	}
        }
        response.sendRedirect(request.getContextPath() + "/datelist");
    }
 
}