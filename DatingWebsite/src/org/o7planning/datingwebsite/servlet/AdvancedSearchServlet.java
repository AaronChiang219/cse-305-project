package org.o7planning.datingwebsite.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
 
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
import org.o7planning.datingwebsite.beans.ProfileAccount;
import org.o7planning.datingwebsite.utils.DBUtils;
import org.o7planning.datingwebsite.utils.MyUtils;
 
@WebServlet(urlPatterns = { "/advsearch"})
public class AdvancedSearchServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
 
    public AdvancedSearchServlet() {
        super();
    }
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Connection conn = MyUtils.getStoredConnection(request);
        List<ProfileAccount> list = null;
        String type = request.getParameter("type");
        System.out.println();
        System.out.println(type);
        if (type==null) {
			RequestDispatcher dispatcher = request.getServletContext()
					.getRequestDispatcher("/WEB-INF/views/advsearchView.jsp");
			dispatcher.forward(request, response);
			return;
        } else if (type.equals("dates")) {
        	try {
        		System.out.println("inside dates");
        		list = DBUtils.queryDateProfiles(conn);
        		request.setAttribute("profileList", list);
				RequestDispatcher dispatcher = request.getServletContext()
						.getRequestDispatcher("/WEB-INF/views/searchView.jsp");
				dispatcher.forward(request, response);
        	} catch (SQLException e) {
        		e.printStackTrace();
        	}
        } else if (type.equals("active")) {
        	try {
        		list = DBUtils.queryActiveProfiles(conn);
        		request.setAttribute("profileList", list);
				RequestDispatcher dispatcher = request.getServletContext()
						.getRequestDispatcher("/WEB-INF/views/searchView.jsp");
				dispatcher.forward(request, response);
        	} catch (SQLException e) {
        		e.printStackTrace();
        	}
        } else if (type.equals("rated")) {
        	try {
        		list = DBUtils.queryRatedProfiles(conn);
        		request.setAttribute("profileList", list);
				RequestDispatcher dispatcher = request.getServletContext()
						.getRequestDispatcher("/WEB-INF/views/searchView.jsp");
				dispatcher.forward(request, response);
        	} catch (SQLException e) {
        		e.printStackTrace();
        	}
        }
        return;


    }
 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Connection conn = MyUtils.getStoredConnection(request);
        List<ProfileAccount> list = null;
        String gender = request.getParameter("gender");
        String ages = request.getParameter("ages");
        String agee = request.getParameter("agee");
        String hobbies = request.getParameter("hobbies");
        String heights = request.getParameter("heights");
        String heighte = request.getParameter("heighte");
        String weights = request.getParameter("weights");
        String weighte = request.getParameter("weighte");
        String hair = request.getParameter("hair");
        String query = "Select * from profile where ";
        if (gender!=null)
        	query += "M_F = " + "'"+ gender + "'" + " and ";
        if (ages!="" && agee!="")
        	query += "age between " + ages + " and " + agee + " and ";
        if (hobbies!="")
        	query += "instr(hobbies, " +"'"+ hobbies+"'" + ")" + " and ";
        if (heights!="" && heighte!="")
        	query += "height between " + heights + " and " + heighte + " and ";
        if (weights!="" && weights!="")
        	query += "weight between " + weights + " and " + weighte + " and ";
        if (hair!="")
        	query += "instr(haircolor, " + "'" + hair + "'" + ")" + " and ";
        query += "1=1";
        try {
			list = DBUtils.queryAdvProfiles(conn, query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        request.setAttribute("profileList", list);
		RequestDispatcher dispatcher = request.getServletContext()
				.getRequestDispatcher("/WEB-INF/views/searchView.jsp");
		dispatcher.forward(request, response);
	
    }
 
}