package org.o7planning.datingwebsite.servlet;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
 
import org.o7planning.datingwebsite.beans.UserAccount;
import org.o7planning.datingwebsite.beans.ProfileAccount;
import org.o7planning.datingwebsite.utils.DBUtils;
import org.o7planning.datingwebsite.utils.MyUtils;
 
@WebServlet(urlPatterns = { "/profile", "/profile/*" })
public class ProfileServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
 
    public ProfileServlet() {
        super();
    }
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
 
        // Check User has logged on
        UserAccount loginedUser = MyUtils.getLoginedUser(session);
 
        String User = request.getPathInfo();
        
        if (User!=null) {
			User = User.substring(1);
        	Connection conn = MyUtils.getStoredConnection(request);
        	ProfileAccount profile = new ProfileAccount();
        	try {
        		profile = DBUtils.findAccount(conn, User);
        	} catch (SQLException e) {
        		e.printStackTrace();
				RequestDispatcher dispatcher //
                    = this.getServletContext().getRequestDispatcher("/WEB-INF/views/loginView.jsp");
				dispatcher.forward(request,response);
				return;
        	}
        	if(loginedUser!=null) {
        		String profName = loginedUser.getUserName();
				String uploadFolder = getServletContext().getRealPath("") + "data" + File.separator;
				File directory = new File(uploadFolder + profName);
				List<String> imageUrlList = new ArrayList<String>();
				if (directory.listFiles()!=null) {
					for(File imageFile : directory.listFiles()){
					  String imageFileName = profName + File.separator + imageFile.getName();
					  // add this images name to the list we are building up
					  imageUrlList.add(imageFileName);
					}
					request.setAttribute("imageUrlList", imageUrlList);

				}
			request.setAttribute("user", loginedUser);
        	}
        	request.setAttribute("profile", profile);
			RequestDispatcher dispatcher //
					= this.getServletContext().getRequestDispatcher("/WEB-INF/views/profileView.jsp");
			dispatcher.forward(request, response);
			return;

        } else {
        	if (loginedUser == null) {
        		response.sendRedirect(request.getContextPath() + "/login");
        		return;
        	}
        	response.sendRedirect(request.getContextPath() + "/profile/" + loginedUser.getUserName());
        	return;
        }
    }
 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String Liker = request.getParameter("Liker");
        String Likee = request.getParameter("Likee");
 
        Connection conn = MyUtils.getStoredConnection(request);
        try {
        	DBUtils.likeProfile(conn, Liker, Likee);
        } catch (SQLException e) {
        	e.printStackTrace();
        	return;
        }
		response.sendRedirect(request.getContextPath() + "/likes" );
        return;
    }
 
}