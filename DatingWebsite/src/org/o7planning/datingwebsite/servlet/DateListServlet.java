package org.o7planning.datingwebsite.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
 
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
import org.o7planning.datingwebsite.utils.DBUtils;
import org.o7planning.datingwebsite.utils.MyUtils;
import org.o7planning.datingwebsite.beans.Date;
import org.o7planning.datingwebsite.beans.UserAccount;
 
@WebServlet(urlPatterns = { "/datelist" })
public class DateListServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
 
    public DateListServlet() {
        super();
    }
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	HttpSession session = request.getSession();
        Connection conn = MyUtils.getStoredConnection(request);
 
        String errorString = null;
        List<Date> list = null;
         
        UserAccount loginedUser = MyUtils.getLoginedUser(session);
        if (loginedUser!=null) {
        	try {
        		list = DBUtils.queryDates(conn, loginedUser.getUserName());
        	} catch (SQLException e) {
        		e.printStackTrace();
        		errorString = e.getMessage();
        	}
			request.setAttribute("errorString", errorString);
        	request.setAttribute("dateList", list);
			RequestDispatcher dispatcher //
					= this.getServletContext().getRequestDispatcher("/WEB-INF/views/dateListView.jsp");
			dispatcher.forward(request, response);
			return;
        } else {
			response.sendRedirect(request.getContextPath() + "/login");
			return;
        }

 
    }
 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	HttpSession session = request.getSession();
        Connection conn = MyUtils.getStoredConnection(request);
        String profile1 = request.getParameter("prof1");
        String profile2 = request.getParameter("prof2");
        String time = request.getParameter("time");
        String date = request.getParameter("date");
        try {
        	DBUtils.deleteDate(conn, profile1, profile2, date, time);
        } catch (SQLException e) {
        	e.printStackTrace();
			RequestDispatcher dispatcher //
			= this.getServletContext().getRequestDispatcher("/WEB-INF/views/loginView.jsp");
			dispatcher.forward(request,response);
			return;
        }
        response.sendRedirect(request.getContextPath() + "/datelist");
    }
 
}