package org.o7planning.datingwebsite.servlet;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
 
import org.o7planning.datingwebsite.beans.UserAccount;
import org.o7planning.datingwebsite.beans.ProfileAccount;
import org.o7planning.datingwebsite.utils.DBUtils;
import org.o7planning.datingwebsite.utils.MyUtils;
 
@WebServlet(urlPatterns = { "/crdate", "/crdate*"})
public class DateCreateServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
 
    public DateCreateServlet() {
        super();
    }
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
 
		Connection conn = MyUtils.getStoredConnection(request);
		String prof1 = request.getParameter("prof1");
		String prof2 = request.getParameter("prof2");
		request.setAttribute("prof1", prof1);
		request.setAttribute("prof2", prof2);
		RequestDispatcher dispatcher //
				= this.getServletContext().getRequestDispatcher("/WEB-INF/views/dateCreateView.jsp");
		dispatcher.forward(request, response);
		return;
    }
 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
		HttpSession session = request.getSession();
 
		Connection conn = MyUtils.getStoredConnection(request);
		String prof1 = request.getParameter("prof1");
		String prof2 = request.getParameter("prof2");
		String time = request.getParameter("time");
		String location = request.getParameter("location");
		System.out.println(prof1);
		System.out.println(prof2);
		System.out.println(time);
		System.out.println(location);

		try {
			DBUtils.addDate(conn, prof1, prof2, time, location);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		response.sendRedirect(request.getContextPath() + "/datelist");
		return;
 
    }
 
}