package org.o7planning.datingwebsite.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
 
import org.o7planning.datingwebsite.beans.UserAccount;
import org.o7planning.datingwebsite.beans.ProfileAccount;
import org.o7planning.datingwebsite.utils.DBUtils;
import org.o7planning.datingwebsite.utils.MyUtils;
 
@WebServlet(urlPatterns = { "/actedit"})
public class AccountEditServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
 
    public AccountEditServlet() {
        super();
    }
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	//modify to get all attributes of current user
        HttpSession session = request.getSession();
 
        // Check User has logged on
        UserAccount loginedUser = MyUtils.getLoginedUser(session);
 
        if (loginedUser!=null) {
        	request.setAttribute("account", loginedUser);
			RequestDispatcher dispatcher //
					= this.getServletContext().getRequestDispatcher("/WEB-INF/views/accountEditView.jsp");
			dispatcher.forward(request, response);
			return;
        } else {
        	response.sendRedirect(request.getContextPath() + "/login");
        }
        return;
    }
 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
		HttpSession session = request.getSession();
		Connection conn = MyUtils.getStoredConnection(request);
        UserAccount loginedUser = MyUtils.getLoginedUser(session);
		String ProfileID = loginedUser.getUserName();
		String FirstName = request.getParameter("firstName");
		String LastName = request.getParameter("lastName");
		String password = request.getParameter("password");
		String ccn = request.getParameter("ccn");
		String Street = request.getParameter("street");
		String City = request.getParameter("city");
		String State = request.getParameter("state");
		int zip = Integer.parseInt(request.getParameter("zip"));
		String Email = request.getParameter("email");
		String Phone = request.getParameter("phone");
		String PPP = request.getParameter("ppp");
    	try {
			DBUtils.updateAccount(conn, ProfileID, FirstName, LastName, password, ccn,  Street, City, State, zip, Email, Phone, PPP);
			loginedUser.setFirstName(FirstName);
			loginedUser.setLastName(LastName);
			loginedUser.setStreet(Street);
			loginedUser.setCity(City);
			loginedUser.setState(State);
			loginedUser.setZip(zip);
			loginedUser.setEmail(Email);
			loginedUser.setPhone(Phone);
			loginedUser.setPpp(PPP);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		response.sendRedirect(request.getContextPath() + "/account");
		return;


    }
 
}