package org.o7planning.datingwebsite.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
 
import org.o7planning.datingwebsite.beans.UserAccount;
import org.o7planning.datingwebsite.utils.DBUtils;
import org.o7planning.datingwebsite.utils.MyUtils;
 
@WebServlet(urlPatterns = { "/account"})
public class AccountServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
 
    public AccountServlet() {
        super();
    }
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
 
        // Check User has logged on
        UserAccount loginedUser = MyUtils.getLoginedUser(session);
 
		if (loginedUser == null) {
			response.sendRedirect(request.getContextPath() + "/login");
			return;
		} else {
			request.setAttribute("account", loginedUser);
			RequestDispatcher dispatcher //
					= this.getServletContext().getRequestDispatcher("/WEB-INF/views/accountView.jsp");
			dispatcher.forward(request, response);
			return;
		}
    }
 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	doGet(request, response);
    }
 
}