package org.o7planning.datingwebsite.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
 
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
import org.o7planning.datingwebsite.utils.DBUtils;
import org.o7planning.datingwebsite.utils.MyUtils;
import org.o7planning.datingwebsite.beans.Date;
import org.o7planning.datingwebsite.beans.UserAccount;
 
@WebServlet(urlPatterns = { "/eplist" })
public class EpListServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
 
    public EpListServlet() {
        super();
    }
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	HttpSession session = request.getSession();
        Connection conn = MyUtils.getStoredConnection(request);
 
        String errorString = null;
        List<UserAccount> list = null;
         
        UserAccount loginedUser = MyUtils.getLoginedUser(session);
        if (loginedUser!=null) {
        	try {
        		list = DBUtils.queryEmployees(conn);
        	} catch (SQLException e) {
        		e.printStackTrace();
        		errorString = e.getMessage();
        	}
			request.setAttribute("errorString", errorString);
        	request.setAttribute("userList", list);
			RequestDispatcher dispatcher //
					= this.getServletContext().getRequestDispatcher("/WEB-INF/views/employeeListView.jsp");
			dispatcher.forward(request, response);
			return;
        } else {
			response.sendRedirect(request.getContextPath() + "/login");
			return;
        }

 
    }
 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
 
}