package org.o7planning.datingwebsite.beans;

public class ProfileAccount {

	   public static final String GENDER_MALE ="M";
	   public static final String GENDER_FEMALE = "F";
	   
	   private String userName;
	   private String picture;
	   private int age;
	   private int rangestart;
	   private int rangeend;
	   private String gender;
	   private String hobbies;
	   private int height;
	   private int weight;
	   private String hair;
	   
	   

	   public ProfileAccount() {
	   }
	   
	   public String getUserName() {
	       return userName;
	   }

	   public void setUserName(String userName) {
	       this.userName = userName;
	   }

	   public String getGender() {
	       return gender;
	   }

	   public void setGender(String gender) {
	       this.gender = gender;
	   }

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getRangestart() {
		return rangestart;
	}

	public void setRangestart(int rangestart) {
		this.rangestart = rangestart;
	}

	public int getRangeend() {
		return rangeend;
	}

	public void setRangeend(int rangeend) {
		this.rangeend = rangeend;
	}

	public String getHobbies() {
		return hobbies;
	}

	public void setHobbies(String hobbies) {
		this.hobbies = hobbies;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public String getHair() {
		return hair;
	}

	public void setHair(String hair) {
		this.hair = hair;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}


	}
