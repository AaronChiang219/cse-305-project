package org.o7planning.datingwebsite.beans;

public class UserAccount {

	   
	   private String userName;
	   private String password;
	   private String FirstName;
	   private String LastName;
	   private String Street;
	   private String City;
	   private String State;
	   private int zip;
	   private String email;
	   private String phone;
	   private String ppp;
	   private String role;
	   private double wage;
	   private String ssn;
	   

	   public UserAccount() {
	       
	   }
	   
	   public String getUserName() {
	       return userName;
	   }

	   public void setUserName(String userName) {
	       this.userName = userName;
	   }

	   public String getPassword() {
	       return password;
	   }

	   public void setPassword(String password) {
	       this.password = password;
	   }


	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getStreet() {
		return Street;
	}

	public void setStreet(String street) {
		Street = street;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getState() {
		return State;
	}

	public void setState(String state) {
		State = state;
	}

	public int getZip() {
		return zip;
	}

	public void setZip(int zip) {
		this.zip = zip;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPpp() {
		return ppp;
	}

	public void setPpp(String ppp) {
		this.ppp = ppp;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public double getWage() {
		return wage;
	}

	public void setWage(double wage) {
		this.wage = wage;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	}