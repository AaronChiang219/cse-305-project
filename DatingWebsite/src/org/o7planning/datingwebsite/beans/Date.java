package org.o7planning.datingwebsite.beans;

public class Date {

	private String profile1;
	private String profile2;
	private String date;
	private String time;
	private String datetime;
	private String CustRep;
	private String comment;
	private String location;
	private float cost;
	private int rating1;
	private int rating2;
	

	public Date() {
		
	}
	public String getProfile2() {
		return profile2;
	}
	public void setProfile2(String profile2) {
		this.profile2 = profile2;
	}
	public String getProfile1() {
		return profile1;
	}
	public void setProfile1(String profile1) {
		this.profile1 = profile1;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getCustRep() {
		return CustRep;
	}
	public void setCustRep(String custRep) {
		CustRep = custRep;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public float getCost() {
		return cost;
	}
	public void setCost(float cost) {
		this.cost = cost;
	}
	public int getRating1() {
		return rating1;
	}
	public void setRating1(int rating1) {
		this.rating1 = rating1;
	}
	public int getRating2() {
		return rating2;
	}
	public void setRating2(int rating2) {
		this.rating2 = rating2;
	}
	public String getDatetime() {
		return datetime;
	}
	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}
}
