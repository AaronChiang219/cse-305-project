package org.o7planning.datingwebsite.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.o7planning.datingwebsite.beans.Date;
import org.o7planning.datingwebsite.beans.Product;
import org.o7planning.datingwebsite.beans.UserAccount;
import org.o7planning.datingwebsite.beans.ProfileAccount;

public class DBUtils {

	public static UserAccount findUser(Connection conn, //
			String userName, String password) throws SQLException {

		String sql = "Select * from profile a, person b, user c" //
				+ " where a.ProfileID = ? and b.Password= ? and a.ownerSSN=b.SSN and b.SSN = c.SSN";

		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, userName);
		pstm.setString(2, password);
		ResultSet rs = pstm.executeQuery();

		if (rs.next()) {
			String FirstName = rs.getString("FirstName");
			String LastName = rs.getString("LastName");
			String Street = rs.getString("Street");
			String City = rs.getString("City");
			String State = rs.getString("State");
			int zip = rs.getInt("zipcode");
			String email = rs.getString("email");
			String phone = rs.getString("telephone");
			String ppp = rs.getString("PPP");
			UserAccount user = new UserAccount();
			user.setUserName(userName);
			user.setPassword(password);
			user.setFirstName(FirstName);
			user.setLastName(LastName);
			user.setStreet(Street);
			user.setCity(City);
			user.setState(State);
			user.setZip(zip);
			user.setEmail(email);
			user.setPhone(phone);
			user.setPpp(ppp);
			return user;
		}
		return null;
	}
	public static UserAccount findEmployee(Connection conn, //
			String userName, String password) throws SQLException {

		String sql = "Select * from person a,  employee b" //
				+ " where a.Email = ? and a.Password= ? and a.SSN=b.SSN ";

		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, userName);
		pstm.setString(2, password);
		ResultSet rs = pstm.executeQuery();

		if (rs.next()) {
			String FirstName = rs.getString("FirstName");
			String LastName = rs.getString("LastName");
			String Street = rs.getString("Street");
			String City = rs.getString("City");
			String State = rs.getString("State");
			int zip = rs.getInt("zipcode");
			int wage = rs.getInt("HourlyRate");
			String email = rs.getString("email");
			String phone = rs.getString("telephone");
			String role = rs.getString("role");
			String ssn = rs.getString("ssn");
			UserAccount user = new UserAccount();
			user.setUserName(userName);
			user.setPassword(password);
			user.setFirstName(FirstName);
			user.setLastName(LastName);
			user.setWage(wage);
			user.setStreet(Street);
			user.setCity(City);
			user.setState(State);
			user.setZip(zip);
			user.setEmail(email);
			user.setPhone(phone);
			user.setRole(role);
			user.setSsn(ssn);
			return user;
		}
		return null;
	}

	public static Date findDate(Connection conn, //
			String profile1, String profile2, String date, String time) throws SQLException {
		String sql = "Select * from date where profile1 = ? and profile2 = ? and Date_Time=?";
		PreparedStatement pstm = conn.prepareStatement(sql);
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		java.util.Date temp = null;
		try {
			temp = df.parse(date + " " + time);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Timestamp tstp = new Timestamp(temp.getTime());
		pstm.setString(1, profile1);
		pstm.setString(2, profile2);
		pstm.setTimestamp(3, tstp);
		
		ResultSet rs = pstm.executeQuery();
		if (rs.next()) {
			System.out.println("Inside ResultSet");
			String prof1 = rs.getString("Profile1");
			String prof2 = rs.getString("Profile2");
			String custrep = rs.getString("CustRep");
			Timestamp timestmp = rs.getTimestamp("Date_Time");
			String da = rs.getDate("Date_Time").toString();
			String ti = rs.getTime("Date_Time").toString();
			String location = rs.getString("Location");
			float fee = rs.getFloat("BookingFee");
			String comments = rs.getString("Comments");
			int rating1 = rs.getInt("User1Rating");
			int rating2 = rs.getInt("User2Rating");

			Date d = new Date();
			d.setComment(comments);
			d.setTime(ti);
			d.setDate(da);
			d.setProfile1(prof1);
			d.setProfile2(prof2);
			d.setCustRep(custrep);
			d.setDatetime(timestmp.toString());
			d.setLocation(location);
			d.setRating1(rating1);
			d.setRating2(rating2);
			d.setCost(fee);
			return d;
		}
		return null;
	}

	public static boolean deleteDate(Connection conn, //
			String profile1, String profile2, String date, String time) throws SQLException {
		String sql = "Delete from date where profile1 = ? and profile2 = ? and Date_Time=?";
		PreparedStatement pstm = conn.prepareStatement(sql);
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		java.util.Date temp = null;
		try {
			temp = df.parse(date + " " + time);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Timestamp tstp = new Timestamp(temp.getTime());
		pstm.setString(1, profile1);
		pstm.setString(2, profile2);
		pstm.setTimestamp(3, tstp);
		
		try {
			pstm.executeUpdate();
			return true;
		} catch (SQLException e) {
			return false;
		}
	}

	public static boolean deleteEmployee(Connection conn, String email, String password) throws SQLException {
		String sql = "delete from employee where SSN in (SELECT SSN from person where Email = ? and Password = ?)";
		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, email);
		pstm.setString(2, password);
		try {
			pstm.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean addDate(Connection conn, //
			String profile1, String profile2, String Date_Time, String Location) throws SQLException{
		String sql = "Insert into date (Profile1, Profile2, Date_Time, Location) values (?,?,?,?)";
		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, profile1);
		pstm.setString(2, profile2);
		pstm.setString(3, Date_Time);
		pstm.setString(4, Location);
		try {
			pstm.executeUpdate();
			return true;
		} catch (SQLException e) {
			return false;
		}
	}
	
	public static boolean removeLike(Connection conn, //
			String Liker, String Likee) throws SQLException {
		String sql = "Delete from likes where Liker=? and Likee=?";
		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, Liker);
		pstm.setString(2, Likee);
		try {
			pstm.executeUpdate();
			return true;
		} catch (SQLException e) {
			return false;
		}
	}

	public static boolean ValidUserName(Connection conn, //
			String userName) throws SQLException{
		String sql = "Select * from profile a where a.ProfileID = ?";
		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, userName);
		ResultSet rs = pstm.executeQuery();
		if (rs.next()) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean ValidSSN(Connection conn, //
			String SSN) throws SQLException{
		String sql = "Select * from person a where a.SSN = ?";
		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, SSN);
		ResultSet rs = pstm.executeQuery();
		if (rs.next()) {
			return true;
		} else {
			return false;
		}
	}

	public static ProfileAccount findAccount(Connection conn, //
			String ProfileID) throws SQLException {
		String sql = "Select * from profile a where a.ProfileID = ?";
		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, ProfileID);
		ResultSet rs = pstm.executeQuery();
		if (rs.next()) {
			int age = rs.getInt("Age");
			String picture = rs.getString("ProfilePic");
			int rangestart = rs.getInt("DatingAgeRangeStart");
			int rangeend = rs.getInt("DatingAgeRangeEnd");
			String gender = rs.getString("M_F");
			String hobbies = rs.getString("Hobbies");
			int height = rs.getInt("Height");
			int weight = rs.getInt("Weight");
			String hair = rs.getString("HairColor");
			ProfileAccount profile = new ProfileAccount();
			profile.setAge(age);
			profile.setPicture(picture);
			profile.setGender(gender);
			profile.setHair(hair);
			profile.setHeight(height);
			profile.setHobbies(hobbies);
			profile.setRangeend(rangeend);
			profile.setRangestart(rangestart);
			profile.setUserName(ProfileID);
			profile.setWeight(weight);
			return profile;
			
		}
		return null;
	}
	
	public static boolean createAccount(Connection conn, //
			String userName, String SSN, long CCN, String password) throws SQLException {
		// Person, User, Profile, Account

		String sql = "Insert into person (SSN, Password) values (?,?) ";
		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1,  SSN);
		pstm.setString(2, password);
		
		try {
			pstm.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

        sql = "Insert into user (SSN, DateOfLastAct) values (?,?)";

		Timestamp time = new Timestamp(System.currentTimeMillis());
		pstm = conn.prepareStatement(sql);
		pstm.setString(1, SSN);
		pstm.setTimestamp(2, time);
		
		try {
			pstm.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

		sql = "Insert into profile (ProfileID, OwnerSSN, CreationDate, LastModDate)" //
				+ "values (?,?,?,?)";
		
		pstm = conn.prepareStatement(sql);
		pstm.setString(1, userName);
		pstm.setString(2, SSN);
		pstm.setTimestamp(3,time);
		pstm.setTimestamp(4, time);
		
		try {
			pstm.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

		sql = "Insert into account values (?,?,UUID_SHORT(),?)";
		
		pstm = conn.prepareStatement(sql);
		pstm.setString(1, SSN);
		pstm.setLong(2, CCN);
		pstm.setTimestamp(3,time);
		
		try {
			pstm.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public static UserAccount findUser(Connection conn, String userName) throws SQLException {

		String sql = "Select a.ProfileID, b.Password from profile a, person b" //
				+ " where a.ProfileID = ? and a.ownerSSN=b.SSN";

		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, userName);

		ResultSet rs = pstm.executeQuery();

		if (rs.next()) {
			String password = rs.getString("Password");
			//String gender = rs.getString("Gender");
			UserAccount user = new UserAccount();
			user.setUserName(userName);
			user.setPassword(password);
			return user;
		}
		return null;
	}

	public static List<UserAccount> queryEmployees(Connection conn) throws SQLException {
		String sql = "SELECT * from person a, employee b where a.SSN = b.SSN";
		PreparedStatement pstm = conn.prepareStatement(sql);
		
		ResultSet rs = pstm.executeQuery();
		List<UserAccount> list = new ArrayList<UserAccount>();
		
		while (rs.next()) {
			String firstname = rs.getString("FirstName");
			String lastname = rs.getString("LastName");
			String password = rs.getString("password");
			String Role = rs.getString("role");
			double wage = rs.getDouble("HourlyRate");
			String email = rs.getString("email");
			String phone = rs.getString("telephone");
			UserAccount user = new UserAccount();
			user.setFirstName(firstname);
			user.setLastName(lastname);
			user.setPassword(password);
			user.setRole(Role);
			user.setWage(wage);
			user.setEmail(email);
			user.setPhone(phone);
			list.add(user);
		}
		return list;
	}

	public static List<ProfileAccount> queryRatedProfiles(Connection conn) throws SQLException {
		String sql = "SELECT *, COUNT(*) as count from likes inner join profile on profile.ProfileID = likes.Likee group by Likee order by count DESC";
		PreparedStatement pstm = conn.prepareStatement(sql);

		ResultSet rs = pstm.executeQuery();

		List<ProfileAccount> list = new ArrayList<ProfileAccount>();
		while (rs.next()) {
			String name = rs.getString("ProfileID");
			String picture = rs.getString("ProfilePic");
			int age = rs.getInt("Age");
			int height = rs.getInt("Height");
			int weight = rs.getInt("Weight");

			ProfileAccount profile = new ProfileAccount();
			profile.setAge(age);
			profile.setPicture(picture);
			profile.setUserName(name);
			profile.setHeight(height);
			profile.setWeight(weight);
			
			list.add(profile);
		}
		return list;
	}

	public static List<ProfileAccount> queryActiveProfiles(Connection conn) throws SQLException {

		String sql = "SELECT *, COUNT(*) as count from ( SELECT Profile1 from date UNION ALL Select Profile2 from date) as tbl inner join profile on profile.ProfileID = Profile1 group by Profile1 order by count DESC";
		PreparedStatement pstm = conn.prepareStatement(sql);

		ResultSet rs = pstm.executeQuery();

		List<ProfileAccount> list = new ArrayList<ProfileAccount>();
		while (rs.next()) {
			String name = rs.getString("ProfileID");
			String picture = rs.getString("ProfilePic");
			int age = rs.getInt("Age");
			int height = rs.getInt("Height");
			int weight = rs.getInt("Weight");

			ProfileAccount profile = new ProfileAccount();
			profile.setAge(age);
			profile.setPicture(picture);
			profile.setUserName(name);
			profile.setHeight(height);
			profile.setWeight(weight);
			
			list.add(profile);
		}
		return list;
	}

	public static boolean likeProfile(Connection conn, String Liker, String Likee) throws SQLException {
		
		String sql = "INSERT into likes values (?,?,?)";
		PreparedStatement pstm = conn.prepareStatement(sql);
		
		Timestamp time = new Timestamp(System.currentTimeMillis());
		System.out.println(Liker);
		System.out.println(Likee);
		
		pstm.setString(1, Liker);
		pstm.setString(2, Likee);
		pstm.setTimestamp(3, time);
		
		try {
			pstm.executeUpdate();
			return true;
		} catch (SQLException e) {
			return false;
		}
	}

	public static List<ProfileAccount> queryDateProfiles(Connection conn) throws SQLException {
		String sql = "SELECT *, COUNT(*) as count from ( SELECT Profile1 from date UNION ALL Select Profile2 from date) as tbl inner join profile on profile.ProfileID = Profile1 group by Profile1 order by count DESC";

		PreparedStatement pstm = conn.prepareStatement(sql);

		ResultSet rs = pstm.executeQuery();

		List<ProfileAccount> list = new ArrayList<ProfileAccount>();
		while (rs.next()) {
			String name = rs.getString("ProfileID");
			String picture = rs.getString("ProfilePic");
			int age = rs.getInt("Age");
			int height = rs.getInt("Height");
			int weight = rs.getInt("Weight");

			ProfileAccount profile = new ProfileAccount();
			profile.setAge(age);
			profile.setPicture(picture);
			profile.setUserName(name);
			profile.setHeight(height);
			profile.setWeight(weight);
			
			list.add(profile);
		}
		return list;
	}

	public static List<ProfileAccount> queryProfiles(Connection conn, String query) throws SQLException {
		String sql = "Select * from profile a where instr(a.ProfileID, ?)";

		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, query);

		ResultSet rs = pstm.executeQuery();

		List<ProfileAccount> list = new ArrayList<ProfileAccount>();
		while (rs.next()) {
			String name = rs.getString("ProfileID");
			String picture = rs.getString("ProfilePic");
			int age = rs.getInt("Age");
			int height = rs.getInt("Height");
			int weight = rs.getInt("Weight");

			ProfileAccount profile = new ProfileAccount();
			profile.setAge(age);
			profile.setPicture(picture);
			profile.setUserName(name);
			profile.setHeight(height);
			profile.setWeight(weight);
			
			list.add(profile);
		}
		return list;
	}
	
	public static List<ProfileAccount> queryAdvProfiles(Connection conn, String query) throws SQLException {
		System.out.println(query);
		String sql = query;
		PreparedStatement pstm = conn.prepareStatement(sql);

		ResultSet rs = pstm.executeQuery();

		List<ProfileAccount> list = new ArrayList<ProfileAccount>();
		while (rs.next()) {
			String name = rs.getString("ProfileID");
			String picture = rs.getString("ProfilePic");
			int age = rs.getInt("Age");
			int height = rs.getInt("Height");
			int weight = rs.getInt("Weight");

			ProfileAccount profile = new ProfileAccount();
			profile.setAge(age);
			profile.setPicture(picture);
			profile.setUserName(name);
			profile.setHeight(height);
			profile.setWeight(weight);
			
			list.add(profile);
		}
		return list;
	}

	public static List<ProfileAccount> queryLikes(Connection conn, String Liker) throws SQLException {
		String sql = "SELECT * FROM likes inner join profile on profile.ProfileID = likes.likee WHERE Liker = ?";

		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, Liker);
		
		ResultSet rs = pstm.executeQuery();
		
		List<ProfileAccount> list = new ArrayList<ProfileAccount>();
		while (rs.next()) {
			String name = rs.getString("ProfileID");
			String picture = rs.getString("ProfilePic");
			int age = rs.getInt("Age");
			int height = rs.getInt("Height");
			int weight = rs.getInt("Weight");

			ProfileAccount profile = new ProfileAccount();
			profile.setAge(age);
			profile.setPicture(picture);
			profile.setUserName(name);
			profile.setHeight(height);
			profile.setWeight(weight);
		
			list.add(profile);
		}

		return list;
		
	}

	public static List<Date> queryDates(Connection conn, String Profile) throws SQLException {
		String sql = "SELECT * FROM date WHERE Profile1 = ? or Profile2 = ?";
		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, Profile);
		pstm.setString(2, Profile);
		
		ResultSet rs = pstm.executeQuery();
		
		List<Date> list = new ArrayList<Date>();
		while (rs.next()) {
			//DateFormat dateFormat = new SimpleDateFormat("hh:mm aa");
			String location = rs.getString("location");
			String profile1 = rs.getString("Profile1");
			String profile2 = rs.getString("Profile2");
			float cost = rs.getFloat("BookingFee");
			String datetime = rs.getTimestamp("Date_Time").toString();
			String date = rs.getDate("Date_Time").toString();
			String time = rs.getTime("Date_Time").toString();
			
			Date d = new Date();
			d.setProfile1(profile1);
			d.setProfile2(profile2);
			d.setDate(date);
			d.setDatetime(datetime);
			d.setTime(time);
			d.setLocation(location);
			d.setCost(cost);
		
			list.add(d);
		}
		return list;
		
	}

	public static boolean updatePic(Connection conn, String Profile, String Picture) throws SQLException {
		String sql = "UPDATE profile SET ProfilePic = ? where ProfileID = ?";
		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, Picture);
		pstm.setString(2, Profile);
		
		try {
			pstm.executeUpdate();
			return true;
		} catch (SQLException e) {
			return false;
		}
		
	}

	public static boolean updateProfile(Connection conn, String ProfileID, String Gender, int Age, String Hobbies, int Height, int Weight, String Hair) throws SQLException {
		String sql = "UPDATE profile SET M_F = ?, Age = ?, Hobbies = ?, Height = ?, Weight = ?, HairColor = ?  where ProfileID = ?";
		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, Gender);
		pstm.setInt(2, Age);
		pstm.setString(3, Hobbies);
		pstm.setInt(4, Height);
		pstm.setInt(5, Weight);
		pstm.setString(6, Hair);
		pstm.setString(7, ProfileID);
		
		try {
			pstm.executeUpdate();
			return true;
		} catch (SQLException e) {
			return false;
		}
		
	}
	
	public static boolean updateAccount(Connection conn, String ProfileID, String FirstName, String LastName, String pass, String ccn, String Street, String City, String State, int zip, String Email, String Phone, String PPP) throws SQLException {
		String sql = "UPDATE person SET FirstName = ?, LastName = ?, Password = ?, Street = ?, City = ?, State = ?, Zipcode = ?, Email = ?, Telephone = ? where " //
				+ "SSN in (SELECT OwnerSSN from Profile where ProfileID = ?)";
		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, FirstName);
		pstm.setString(2, LastName);
		pstm.setString(3, pass);
		pstm.setString(4, Street);
		pstm.setString(5, City);
		pstm.setString(6, State);
		pstm.setInt(7, zip);
		pstm.setString(8, Email);
		pstm.setString(9, Phone);
		pstm.setString(10, ProfileID);
		
		try {
			pstm.executeUpdate();
		} catch (SQLException e) {
			return false;
		}

		sql = "UPDATE account SET CardNumber=? WHERE OwnerSSN in (SELECT OwnerSSN from profile where ProfileID=?)";
		pstm = conn.prepareStatement(sql);
		pstm.setLong(1, Long.parseLong(ccn));
		pstm.setString(2, ProfileID);
		
		try {
			pstm.executeUpdate();
			return true;
		
		} catch (SQLException e) {
			return false;
		}
	}
	
public static boolean updateEmployee(Connection conn, String FirstName, String LastName, String Street, String City, String State, int zip, String Email, String Phone, String Wage, String Role, String SSN) throws SQLException {
		String sql = "UPDATE person SET FirstName = ?, LastName = ?, Street = ?, City = ?, State = ?, Zipcode = ?, Email = ?, Telephone = ? where " //
				+ "SSN = ?";
		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, FirstName);
		pstm.setString(2, LastName);
		pstm.setString(3, Street);
		pstm.setString(4, City);
		pstm.setString(5, State);
		pstm.setInt(6, zip);
		pstm.setString(7, Email);
		pstm.setString(8, Phone);
		pstm.setString(9, SSN);
		
		try {
			pstm.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
		sql = "UPDATE employee SET role = ?, hourlyrate = ? where SSN = ?";
		pstm = conn.prepareStatement(sql);
		pstm.setString(1, Role);
		pstm.setInt(2, (int)Double.parseDouble(Wage));
		pstm.setString(3, SSN);
		
		try {
			pstm.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

	}

public static boolean addEmployee(Connection conn, String FirstName, String LastName, String Street, String City, String State, String Zip, String Email, String Phone, String SSN, String Pass, String Role, String Rate) throws SQLException {
		String sql = "Insert into person values (?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1,SSN);
		pstm.setString(2,Pass);
		pstm.setString(3, FirstName);
		pstm.setString(4, LastName);
		pstm.setString(5, Street);
		pstm.setString(6, City);
		pstm.setString(7, State);
		pstm.setString(8, Zip);
		pstm.setString(9, Email);
		pstm.setString(10, Phone);
		
		try {
			pstm.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
		sql = "Insert into employee values (?,?,?,?)";
		pstm = conn.prepareStatement(sql);		
		java.sql.Date temp = new java.sql.Date(System.currentTimeMillis());
		pstm.setString(1,SSN);
		pstm.setString(2,Role);
		pstm.setDate(3, temp);
		pstm.setInt(4, (int)Double.parseDouble(Rate));
		try {
			pstm.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
	}

public static List<Date> querySalesMonth(Connection conn, String date) throws SQLException {
		String sql = "Select * from date where substring(cast(Date_Time as date),1,7)=?";
		PreparedStatement pstm  = conn.prepareStatement(sql);
		pstm.setString(1, date);
		ResultSet rs = null;
		try {
			rs = pstm.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		List<Date> list = new ArrayList<Date>();
		while (rs.next()) {
			float price = rs.getFloat("BookingFee");
			Date d = new Date();
			d.setCost(price);
			d.setDate(date);
			list.add(d);
		}
		return list;
	}

public static List<Date> querySalesDay(Connection conn, String date) throws SQLException {
		String sql = "Select * from date where cast(Date_Time as date)=?";
		PreparedStatement pstm  = conn.prepareStatement(sql);
		pstm.setString(1, date);
		ResultSet rs = null;
		try {
			rs = pstm.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		List<Date> list = new ArrayList<Date>();
		while (rs.next()) {
			float price = rs.getFloat("BookingFee");
			Date d = new Date();
			d.setCost(price);
			d.setDate(date);
			list.add(d);
		}
		return list;
	}

	public static boolean updateDate(Connection conn, //
			String profile1, String profile2, String date, String time, String rating1, String rating2) throws SQLException {
		String sql = "update date set User1Rating = ?, User2Rating = ? where profile1 = ? and profile2 = ? and Date_Time = ?";
		PreparedStatement pstm = conn.prepareStatement(sql);
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		java.util.Date temp = null;
		System.out.println(profile1);
		System.out.println(profile2);
		System.out.println(date);
		System.out.println(time);
		System.out.println(rating1);
		System.out.println(rating2);
		try {
			temp = df.parse(date + " " + time);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Timestamp tstp = new Timestamp(temp.getTime());
		pstm.setInt(1, Integer.parseInt(rating1));
		pstm.setInt(2, Integer.parseInt(rating2));
		pstm.setString(3, profile1);
		pstm.setString(4, profile2);
		pstm.setTimestamp(5, tstp);
		try {
			pstm.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	public static boolean addComment(Connection conn, //
			String profile1, String profile2, String date, String time, String newcomm) throws SQLException {
		String sql = "update date set Comments = ? where profile1 = ? and profile2 = ? and Date_Time = ?";
		PreparedStatement pstm = conn.prepareStatement(sql);
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		java.util.Date temp = null;
		try {
			temp = df.parse(date + " " + time);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Timestamp tstp = new Timestamp(temp.getTime());
		pstm.setString(1, newcomm);
		pstm.setString(2, profile1);
		pstm.setString(3, profile2);
		pstm.setTimestamp(4, tstp);
		try {
			pstm.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
}
